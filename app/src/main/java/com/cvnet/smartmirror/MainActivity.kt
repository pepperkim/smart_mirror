package com.cvnet.smartmirror

import android.Manifest
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.cvnet.smartmirror.databinding.ActivityMainBinding
import com.cvnet.smartmirror.repo.UserDB
import com.ebelter.scaleblesdk.ScaleBleManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.gun0912.tedpermission.PermissionListener
import com.gun0912.tedpermission.coroutine.TedPermission
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)

        navView.setupWithNavController(navController)

        ScaleBleManager.init(this)
        UserDB.open(this)
        asyncPermissionCheck()
    }

    private fun asyncPermissionCheck() = lifecycleScope.launch{
        TedPermission.create()
            .setPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.BLUETOOTH_SCAN,
                Manifest.permission.BLUETOOTH_ADVERTISE,
                Manifest.permission.BLUETOOTH_CONNECT
            )
            .check()
    }


    override fun onKeyUp(keyCode: Int, event: KeyEvent): Boolean {

        val intent = Intent("key_event_action").apply {
            putExtra("key_event_extra", keyCode)
        }
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)

        return when (keyCode) {
            KeyEvent.KEYCODE_D -> {
//                moveShip(MOVE_LEFT)
                true
            }
            KeyEvent.KEYCODE_F -> {
//                moveShip(MOVE_RIGHT)
                true
            }
            KeyEvent.KEYCODE_J -> {
//                fireMachineGun()
                true
            }
            KeyEvent.KEYCODE_K -> {
//                fireMissile()
                true
            }
            else -> super.onKeyUp(keyCode, event)
        }
    }
}