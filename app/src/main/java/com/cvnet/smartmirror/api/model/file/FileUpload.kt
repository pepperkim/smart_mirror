package com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.file

import java.io.File

class FileUpload {
	data class Request(
		val files: Collection<File>,
		val fileTag: String? = null,
		val groupId: String? = null,
		val thumbnail: Boolean? = null,
		val svcCode: String = "member.faceauth"
	)

	data class Response(val fileGroupId: String, val resultData: Collection<FileInfo>)
}