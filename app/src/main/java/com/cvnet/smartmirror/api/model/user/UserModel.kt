package com.cvnet.smartmirror.api.model.user

data class UserModel(
    var name:String? ,
    var age:Int? ,
    var role:String?,
    var id:Long? = null
)
