package com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.file

data class FileInfo(val fileSeq: Long, val fileGroupId: String,
                    val fileOrgName: String, val fileNm: String,
                    val fileUrl: String, val fileSize: Long)
