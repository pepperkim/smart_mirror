package com.cvnet.smartmirror.api

import android.webkit.MimeTypeMap
import com.cvnet.smartmirror.api.model.auth.Login
import com.cvnet.smartmirror.api.model.common.ApiRequestCommon
import com.cvnet.smartmirror.repo.MemStore
import com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.common.ApiErrorCode
import com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.common.ApiResponse
import com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.file.FileUpload
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber

object RestApiHelper {
	suspend fun login(userId: String?): String? {
		MemStore.Auth.clearToken()
		val res = RestApi.client.runCatching {
			if (userId.isNullOrBlank())
				login(ApiRequestCommon())
			else
				login(Login.Request(userId))
		}.onFailure {
			Timber.w(it)
		}.getOrNull() ?: return null

		if (res.status != ApiErrorCode.SUCCESS || res.payload?.result_value != Login.Response.ResultValue.Y)
			return null.also { Timber.d("Login Response Failed (${res.status}: ${res.message}") }

		MemStore.Auth.updateAccessToken(res.payload.access_token, res.payload.expires_in)
//		MemStore.User.updateUserInfo(res.payload)
		return res.payload.access_token
	}

	@Throws
	fun <T> responseChecker(res: ApiResponse<T>): ApiResponse<T> {
		if (res.status != ApiErrorCode.SUCCESS)
			throw Exception("response error ${res.status}")

		return res
	}

	suspend fun fileUpload(request: FileUpload.Request): ApiResponse<FileUpload.Response> {
		val body = MultipartBody.Builder().apply {
			setType(MultipartBody.FORM)
			addFormDataPart("lang", MemStore.Setting.language)
			addFormDataPart("createId", MemStore.User.userAccount)
			addFormDataPart("svcCode", request.svcCode)
			request.groupId?.let { addFormDataPart("fileGroupId", it) }
			request.thumbnail?.let { addFormDataPart("thumbnailFlag", if (it) "Y" else "N") }
			request.fileTag?.let { addFormDataPart("fileTag", it) }
			request.files.forEach {
				val mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(it.extension)
					?: "application/octet-stream"
				val bodyPart = it.asRequestBody(mime.toMediaTypeOrNull())
				addFormDataPart("file", it.name, bodyPart)
			}
		}.build()

		return RestApi.client
			.runCatching { fileUpload(MemStore.Auth.robotSerial, body.parts) }
			.onFailure { Timber.w(it) }
			.getOrThrow()
	}
}