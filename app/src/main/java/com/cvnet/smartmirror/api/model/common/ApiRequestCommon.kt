package com.cvnet.smartmirror.api.model.common

import com.cvnet.smartmirror.repo.MemStore


open class ApiRequestCommon(
    val lang: String = MemStore.Setting.language,
    val robotSerialNumber: String = MemStore.Auth.robotSerial
)