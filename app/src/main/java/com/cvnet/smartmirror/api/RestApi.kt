package com.cvnet.smartmirror.api

import android.os.SystemClock
import com.cvnet.smartmirror.AppDefine
import com.cvnet.smartmirror.api.model.auth.Login
import com.cvnet.smartmirror.api.model.common.ApiRequestCommon
import com.cvnet.smartmirror.repo.MemStore
import com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.common.ApiResponse
import com.hancom.robotics.homerobot_toki2.service.detector.detectservice.model.api.file.FileUpload
import kotlinx.coroutines.runBlocking
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import timber.log.Timber
import java.util.concurrent.TimeUnit

interface RestApi {
    companion object {
        private const val TAG = "API"

        private fun makeAuthenticatedChain(chain: Interceptor.Chain): Request {
            val appendedAuthHeaderRequest = { accessToken: String ->
                chain.request().newBuilder()
                    .addHeader("AuthorizationTkh2", accessToken)
                    .build()
            }

            // 토큰이 없으면 그냥 호출
            val accessToken = MemStore.Auth.accessToken
            if (accessToken.isBlank())
                return chain.request()


            val currentTime = SystemClock.elapsedRealtime()
            // 토큰이 유효하면 기존 토큰 사용
            if (currentTime < MemStore.Auth.accessTokenExpired - 10 * 60_000)
                return appendedAuthHeaderRequest(accessToken)

            // 신규 토큰 발급
            return runBlocking {
                val userId = MemStore.User.userAccount
                val newToken = RestApiHelper.login(userId) ?: return@runBlocking chain.request()

                return@runBlocking appendedAuthHeaderRequest(newToken)
            }
        }

        private val httpClient = OkHttpClient.Builder()
            .addInterceptor { chain -> chain.proceed(makeAuthenticatedChain(chain)) }
            .addInterceptor(
                HttpLoggingInterceptor { Timber.tag(TAG).d(it) }.setLevel(
                    HttpLoggingInterceptor.Level.BODY))
            .connectTimeout(5L, TimeUnit.SECONDS)
            .build()

        val client: RestApi = Retrofit.Builder()
            .baseUrl(AppDefine.RestfulUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient)
            .build()
            .create(RestApi::class.java)
    }

    @POST("")
    @Multipart
    suspend fun fileUpload(
        @Header("robotSerialNumber") robotSerial: String,
        @Part parts: List<MultipartBody.Part>
    ): ApiResponse<FileUpload.Response>

    @GET
    fun fileDownload(@Url fileUrl: String): Call<ResponseBody>

    @POST("")
    suspend fun login(@Body req: ApiRequestCommon): ApiResponse<Login.Response>
}