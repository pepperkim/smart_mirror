package com.cvnet.smartmirror

import android.app.Application
import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.MemoryCategory
import com.fourlab.roomtest.repo.Pref
import timber.log.Timber
import java.lang.ref.WeakReference

class MainApp : Application() {

    companion object {
        private lateinit var contextRef: WeakReference<Context>
        val context: Context
            get() = contextRef.get()!!
    }

    override fun onCreate() {
        super.onCreate()
        contextRef = WeakReference(this.applicationContext)

        Glide.get(this).setMemoryCategory(MemoryCategory.HIGH)

        object : Timber.DebugTree() {
            override fun createStackElementTag(element: StackTraceElement): String? {
                return super.createStackElementTag(element)
                    ?.let { "[${Thread.currentThread().name}] $it" }
            }
        }.let { Timber.plant(it) }

        Pref.open(this.applicationContext)
    }
}