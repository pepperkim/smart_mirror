package com.cvnet.smartmirror.repo.entity

import androidx.room.Embedded
import androidx.room.Relation

data class UserWithScale(
    @Embedded
    val user : UserEntity,

    @Relation(
        parentColumn = "id",
        entityColumn = "userId"
    )
    val scales : List<ScaleEntity>
)
