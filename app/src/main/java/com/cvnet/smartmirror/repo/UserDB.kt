package com.cvnet.smartmirror.repo

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.cvnet.smartmirror.repo.dao.ScaleDao
import com.cvnet.smartmirror.repo.dao.UserDao
import com.cvnet.smartmirror.repo.entity.ScaleEntity
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.fourlab.roomtest.repo.DBConverter
import com.fourlab.roomtest.repo.Pref
import timber.log.Timber

@Database(version = 1, entities = [UserEntity::class, ScaleEntity::class])
@TypeConverters(
    DBConverter.UserConverter::class,
    DBConverter.ScaleConverter::class
)
abstract class UserDB : RoomDatabase(){
    abstract fun userDao():UserDao
    abstract fun scaleDao(): ScaleDao
    companion object {
        private const val dbName = "user.db"

        @Volatile
        private var instance : UserDB? = null
        val db get() = synchronized(this) { instance ?: throw IllegalStateException("Not Initialized FaceDetectDB") }

        fun open(context: Context) = instance ?: synchronized(this) {
            instance ?: Room.databaseBuilder(context.applicationContext, UserDB::class.java, dbName)
                .build().also { instance = it }
        }

        fun closeDB() = synchronized(this) {
            val instance = instance ?: return@synchronized
            this.instance = null

            instance.close()
        }

        fun restore(context: Context) = synchronized(this) {
            val isOpenDB = instance != null
            try {
                if (!Pref.Instance.isNew) return true.also { Timber.d("No need restore DB") }

                closeDB()

                Pref.Instance.isNew = false
                return true
            } catch (e: Exception) {
                Timber.w(e)
                false
            } finally {
                if (isOpenDB) open(context)
            }
        }

        fun reset(context: Context) = synchronized(this) {
            val isOpenDB = instance != null

            if (!isOpenDB) open(context)
            db.clearAllTables()

            if (!isOpenDB) closeDB()
        }

    }
}