package com.cvnet.smartmirror.repo.dao

import androidx.room.*
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.cvnet.smartmirror.repo.entity.UserWithScale

@Dao
abstract class UserDao {

    @Query("select * from userentity" )
    abstract fun findAll():List<UserEntity>

    @Query("select * from userentity where id = :id")
    abstract fun findOneId(id: Long):UserEntity

    @Query("select * from userentity where name = :name")
    abstract fun findOneName(name: String):UserEntity

    @Insert
    abstract fun insert(userEntity: UserEntity):Long

//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    abstract suspend fun insert(users : List<UserEntity>):Long

    @Update
    abstract fun update(userEntity: UserEntity):Int

    @Delete
    abstract fun delete(userEntity: UserEntity):Int

    @Query("delete from userentity where id = :id")
    abstract fun deleteById(id: Long):Int

    @Transaction
    @Query("select * from userentity where userentity.id = :id")
    abstract fun findAllUserScale(id: Long): List<UserWithScale>

}