package com.cvnet.smartmirror.repo.entity

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(foreignKeys = [
    ForeignKey(entity = UserEntity::class,
    parentColumns = ["id"],
    childColumns = ["userId"],
    onDelete = ForeignKey.CASCADE)
], indices = [
    Index("userId")
])
data class ScaleEntity (
    var date:String? = "",
    var weight: Float? = 0f,
    var userId: Long? = 0,
    @PrimaryKey(autoGenerate = true)
    var scaleId:Long? = null
)