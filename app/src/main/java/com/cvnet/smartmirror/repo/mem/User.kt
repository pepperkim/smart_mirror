package com.cvnet.smartmirror.repo.mem

import com.cvnet.smartmirror.api.model.auth.Login
import com.cvnet.smartmirror.repo.entity.UserEntity

class User {

    var userAccount: String = ""
        private set
    var userId: Long = 0
        private set
    var userName: String = ""
        private set

//    fun updateUserInfo(user: Login.Response) {
    fun updateUserInfo(user: UserEntity) {
        with(user) {
            userId = id!!
            userName = name!!
//            robotUserId = robotUserSeq
        }
    }
}