package com.cvnet.smartmirror.repo.mem

import android.bluetooth.BluetoothDevice

class ScaleDevice {

    var device : BluetoothDevice? = null

    fun updateDevice(device : BluetoothDevice){
        this.device = device
    }
}