package com.cvnet.smartmirror.repo.dao

import androidx.room.*
import com.cvnet.smartmirror.repo.entity.ScaleEntity

@Dao
abstract class ScaleDao {
    @Query("select * from scaleentity where userId = :id")
    abstract fun findAll(id: Long): List<ScaleEntity>

    @Query("select * from scaleentity where scaleId = :id")
    abstract fun findOneId(id: Long): ScaleEntity

    @Query("select * from scaleentity where userId = :id and date = :date")
    abstract fun findOneToday(id: Long, date: String): ScaleEntity?

    @Insert
    abstract fun insert(scaleEntity: ScaleEntity):Long

    @Update
    abstract fun update(scaleEntity: ScaleEntity):Int

    @Delete
    abstract fun delete(scaleEntity: ScaleEntity):Int

    @Query("delete from scaleentity where scaleId = :id")
    abstract fun deleteById(id: Long):Int

}