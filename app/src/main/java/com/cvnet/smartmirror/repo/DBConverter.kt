package com.fourlab.roomtest.repo

import androidx.room.TypeConverter
import com.cvnet.smartmirror.repo.entity.ScaleEntity
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.google.gson.Gson

object DBConverter {

    class UserConverter {
        @TypeConverter
        fun listToJson(value: List<UserEntity>?) = value.let{
            Gson().toJson(it)
        }

        @TypeConverter
        fun jsonToList(value : String) = value.let{
            Gson().fromJson(it, Array<UserEntity>::class.java)
        }
    }

    class ScaleConverter {
        @TypeConverter
        fun listToJson(value: List<ScaleEntity>?) = value.let{
            Gson().toJson(it)
        }

        @TypeConverter
        fun jsonToList(value : String) = value.let{
            Gson().fromJson(it, Array<ScaleEntity>::class.java)
        }
    }
}