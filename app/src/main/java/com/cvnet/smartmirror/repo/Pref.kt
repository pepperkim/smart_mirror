package com.fourlab.roomtest.repo

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit

class Pref private constructor(context:Context){
    companion object {
        private var instance: Pref? = null
        val Instance get() = instance ?: throw IllegalStateException("Pref is not opened.")

        fun open(context: Context) = instance ?: synchronized(this) {
            instance ?: Pref(context).also { instance = it }
        }
    }

    private val pref: SharedPreferences = context.getSharedPreferences("setting", Context.MODE_PRIVATE)

    fun clear() = pref.edit { clear() }

    var isNew
        get() = pref.getBoolean("isNew", false)
        set(value) = pref.edit { putBoolean("isNew", value) }

    var isPaired
        get() = pref.getBoolean("isPaired", false)
        set(value) = pref.edit { putBoolean("isPaired", value) }

    var pairDevice
        get() = pref.getString("pairDevice", "")
        set(value) = pref.edit{putString("pairDevice", value)}
}