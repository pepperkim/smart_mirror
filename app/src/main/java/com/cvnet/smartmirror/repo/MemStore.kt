package com.cvnet.smartmirror.repo

import com.cvnet.smartmirror.repo.mem.Auth
import com.cvnet.smartmirror.repo.mem.ScaleDevice
import com.cvnet.smartmirror.repo.mem.Setting
import com.cvnet.smartmirror.repo.mem.User

object MemStore {
	val Auth = Auth()
	val Setting = Setting()
	val User = User()
	val ScaleDevice = ScaleDevice()
}