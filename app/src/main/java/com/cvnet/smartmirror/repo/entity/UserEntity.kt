package com.cvnet.smartmirror.repo.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserEntity(
    var name:String? ,
    var age:Int? ,
    var role:String?,
    @PrimaryKey(autoGenerate = true)
    var id:Long? = null
)
