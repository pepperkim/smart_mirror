package com.cvnet.smartmirror.utils;

import android.content.Context;
import android.util.TypedValue;

/**
 * Created by Mark on 2016-02-17.
 */
public class DpPixel {
    public static int dpToPixel(Context context, int DP) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, DP, context.getResources().getDisplayMetrics());
        return (int) px;
    }
}
