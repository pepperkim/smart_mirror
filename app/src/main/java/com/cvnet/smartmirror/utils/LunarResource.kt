package com.cvnet.smartmirror.utils

import com.cvnet.smartmirror.MainApp
import java.util.*

/**
 * 현재 날짜 기준 음력 날짜 반환 후 ,
 * 음력 날짜 기준 달 이미지 리소스 아이디 반환
 */
class LunarResource {

    fun getLunarResourceId(): Int {
        // 음력 계산 클래스 생성
        val lunarCalendar = KoreanLunarCalendar.getInstance()
        // 현재시간 기준 달력 생성
        val calendar = Calendar.getInstance()
        // 음력 달력에 현재 날짜 대입
        lunarCalendar.setSolarDate(
            calendar[Calendar.YEAR], calendar[Calendar.MONDAY] + 1,
            calendar[Calendar.DAY_OF_MONTH]
        )
        // 내부 리소스 이름에 음력 날짜 대입
        val resName = "@drawable/lunar_" + lunarCalendar.lunarDay
        // 내부 리소스 접근을 위한 패키지명 추출
        val packName: String = MainApp.context.packageName
        // 내부 리소스 파일 이름으로 리소스 아이디 추출하여 반환
        return MainApp.context.resources
            .getIdentifier(resName, "drawable", packName)
    }

}