package com.cvnet.smartmirror.utils.chart;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

public class MyValueFormatter implements ValueFormatter
{

    private final DecimalFormat mFormat;
    private Boolean hideMode = false;

    public MyValueFormatter(Boolean mode) {
        mFormat = new DecimalFormat("###,###,###,##0.0");
        hideMode = mode;
    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        String formattedValue;
        if(hideMode){
            formattedValue = entry.getData().toString();
        }else{
            formattedValue = mFormat.format(value) + "Kg";
        }
        return formattedValue;
    }
}
