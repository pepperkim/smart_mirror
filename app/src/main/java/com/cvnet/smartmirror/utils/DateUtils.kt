package com.cvnet.smartmirror.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    @SuppressLint("SimpleDateFormat")
    fun getTimeStampToDate(time:Long, simple:Boolean = false):String{
        val date = Date(time)
        val formatLong = SimpleDateFormat("yyyy-mm-dd hh:mm:ss")
        val formatSimple = SimpleDateFormat("mm/dd")
        return if(simple){
            formatSimple.format(date)
        }else{
            formatLong.format(date)
        }
    }

    @SuppressLint("SimpleDateFormat")
    fun isToday(time:Long):Boolean{
        val now = System.currentTimeMillis()
        val dateToday = Date(now)
        val dateCompare = Date(time)
        val formatCompare = SimpleDateFormat("yyyy-mm-dd")
        formatCompare.format(dateCompare)
        formatCompare.format(dateToday)
        val compare = dateCompare.compareTo(dateToday)
        return compare == 0
    }
}