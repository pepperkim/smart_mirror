package com.cvnet.smartmirror.utils

import timber.log.Timber
import java.lang.Exception
import kotlin.math.pow

/**
 * 체중계 측정 데이터 기준치 산정 모듈 클래스
 * 성별, 나이, 측정치 기준으로 미달, 평균, 초과 레벨 값 반환
 */
object HealthInfoEvaluation {
//            val btId: String? = null // 블루투스 아이디
//            val age = 0 // 나이
//            val sex = 0 // 성별

//            val mac: String? = null // ?
//            val measureTime: String? = null // ?
//            val resistance = 0f // ?

//            val weight = 0f // 체중

//               val fat = 0f // 체지방
//               val bmi = 0f // bmi
//               val waterRate = 0f // 체수분
//               val bmr = 0f // 기초대사량

//               val boneVolume = 0f // 뼈 볼륨 - 골밀도?
//               val muscleVolume = 0f // 근육량
//            val visceralFat = 0f // 내장 지방

    enum class StandardFatMale (val data: Int) {
        LOW(0),
        BASIC(1),
        HIGH(2),
    }

    // 체중
    val weightStandardDataList = arrayOf(18.5, 25, 30)
    // BMI
    val bmiStandardDataList = arrayOf(18.5, 23, 25, 30)
    // 내장 지방
    val visceralFatStandardDataList = arrayOf(1, 10, 15)

    // 체지방
    val fatStandardList:Array<Array<Any>> = arrayOf(
        arrayOf(0,18, 39, 8f, 20f, 25f),
        arrayOf(0,40, 59, 11f, 22f, 28f),
        arrayOf(0,60, 159, 13f, 25f, 30f),
        arrayOf(1,18, 39, 20f, 33f, 39f),
        arrayOf(1,40, 59, 22f, 34f, 40f),
        arrayOf(1,60, 159, 23f, 36f, 42f),
    )

    // 기초대사량
    val bmrStandardDataList:Array<Array<Any>> = arrayOf(
        arrayOf(0, 18, 39, 1359.8f, 2096.2f),
        arrayOf(0, 40, 59, 1367.4f, 1971.6f),
        arrayOf(0, 60, 150, 1178.5f, 1809.1f),
        arrayOf(1, 18, 39, 1078.5f, 1544.5f),
        arrayOf(1, 40, 59, 1090.9f, 1542.7f),
        arrayOf(1, 60, 150, 1023.9f, 1481.1f),
    )

    // 근육량
    val muscleStandardDataList:Array<Array<Any>> = arrayOf(
        arrayOf(0, 1, 159, 38.5, 46.5),
        arrayOf(0, 160, 170, 44, 52.4),
        arrayOf(0, 171, 250, 49.4, 59.4),
        arrayOf(1, 1, 149, 21.9, 34.7),
        arrayOf(1, 150, 160, 32.9, 37.5),
        arrayOf(1, 161, 250, 36.5, 42.5),
    )

    // 골밀도
    val boneStandardDataList:Array<Array<Any>> = arrayOf(
        arrayOf(0, 2.5, 59.9, 2.1, 2.9),
        arrayOf(0, 60, 74.9, 2.5, 3.3),
        arrayOf(0, 75, 150, 2.7, 3.7),
        arrayOf(1, 2.5, 44.9, 1.5, 2.1),
        arrayOf(1, 45, 59.9, 2.1, 2.9),
        arrayOf(1, 60, 150, 2.1, 2.9),
    )

    // 체수분
    val waterRateStandardDataList:Array<Array<Any>> = arrayOf(
        arrayOf(0, 0, 150, 50, 65),
        arrayOf(1, 0, 150, 45, 60)
    )

    // 내장지방
    fun getStandardVisceralFat(_fat:Float?):Int{
        _fat?.let {
            if(it < 0){
                return -1
            }
            return when(it){
                in 1.0..9.0 ->0
                in 10.0..15.0 ->1
                else ->2
            }
        }
        return 0
    }

    // BMI
    fun getStandardBMI( _bmi:Float?):Int {
        _bmi?.let{
            if(it <0){
                return -1
            }
            return when(it.toDouble()){
                in 0.0..18.4 ->  0
                in 18.5..22.9 ->  1
                in 23.0..24.9 ->  2
                in 25.0..29.9 ->  3
                else ->  4
            }
        }
        return 0
    }

    //체수분
    fun getStandardWaterRate( info: Long, sex: Long, listData: ArrayList<ArrayList<Long>>) :Int{
        for(col in listData){
            if(col[0] == sex){
                return if(info <col[1]){
                    1
                }else if( col[1] > info && info <= col[2]){
                    2
                }else if(info <=col[2]){
                    3
                }else{
                    0
                }
            }else{
                continue
            }
        }
        return 0
    }

    // STT : choice sex - three example - three example
    fun getStandardSTT( standard:Long ,info:Long,  sex:Long = 0, listData:ArrayList<ArrayList<Long>>) : Int {
        for(col in listData){
            if(col[0] != sex){
                continue
            }else{
                if( col[1] < standard && standard <= col[2]){
                    if(info < col[3]){
                        return 0
                    }else if( col[3] < info && info <= col[4]){
                        return 1
                    }else if( info >col[4]) {
                        return 2
                    }
                }else{
                    continue
                }
            }
        }
        return 0
    }

    // 복수 조건별 등급 반환 ( 체지방, 기초대사량, 근육량, 골격량
    fun getStandardST(sex:Int = 0,  age:Int ,info:Float,   listData:Array<Array<Any>>) : Int {
        try{
            for(col in listData){
                val _min = col[1] as Int
                val _max = col[2] as Int
                if(col[0] == sex && age in _min.._max) {
                    for (i in 3 until col.size) {
                        val _compare = col[i] as Float
                        if(info < _compare){
                            return i-3
                        }else if( info >= _compare){
                            continue
                        }
                    }
                    return col.size -3
                }
            }
        }catch(err:Exception){
            Timber.d("getStandardST : $err")

        }
        return 0
    }

    // 체중 등급 반환
    fun getScaleStandard(_height:Long, _scale:Long ):Int{
        val bmiStandard = listOf(18.5, 25, 30)
        for(i in 0..bmiStandard.size){
            val st = bmiStandard[i]
            val standardScale = st.toLong() * _height.toDouble().pow(2.0)
            if(standardScale < _scale){
                return i
            }else if (standardScale > _scale){
                return i+1
            }
        }
        return 0
    }

    fun evaluationString(_lvl:Int):String{
        return when(_lvl){
            0 -> "조금 부족해요"
            1 -> "정상이에요"
            2 -> "조금 높아요"
            3 -> "높은 편이에요"
            else -> "-"
        }
    }
}