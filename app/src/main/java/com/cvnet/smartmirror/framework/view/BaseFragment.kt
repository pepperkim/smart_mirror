package com.cvnet.smartmirror.framework.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager

abstract class BaseFragment<T: ViewDataBinding, V> : Fragment() {

    private lateinit var broadcastManager: LocalBroadcastManager

    private val keyDownReceiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {
            when(val keycode = intent?.getIntExtra("key_event_extra", KeyEvent.KEYCODE_UNKNOWN)){
                KeyEvent.KEYCODE_1 -> {
                    Toast.makeText(requireContext(), "key_event_extra : keycode = $keycode", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /**
     * @return layout resource id
     */
    @get:LayoutRes
    abstract val layoutId: Int

    protected var mRootView: View? = null
    lateinit var viewDataBinding : T
//    protected lateinit var mViewModel : V

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        try{
            viewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
            mRootView = viewDataBinding.root
        }catch (e: IllegalStateException){
            e.printStackTrace()
        }
        return mRootView
    }
}