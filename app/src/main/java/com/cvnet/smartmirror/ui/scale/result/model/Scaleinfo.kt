package com.cvnet.smartmirror.ui.scale.result.model

data class ScaleInfo(
    val user_id:String? = "",
    val scale_num:Float? = 0f,
    val scale_date:String? = ""
)
