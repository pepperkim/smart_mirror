package com.cvnet.smartmirror.ui.healthinfo

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cvnet.smartmirror.ui.scale.result.model.HealthInfo
import com.cvnet.smartmirror.ui.scale.result.model.ScaleInfo

class HealthInfoViewModel : ViewModel() {

    val chartItems = MutableLiveData<MutableList<ScaleInfo>>()

    val healthInoItems = Pair<ObservableField<Boolean>,
            MutableLiveData<MutableList<HealthInfo>>>(ObservableField(false), MutableLiveData(mutableListOf()))

    private val _text = MutableLiveData<String>().apply {
        value = "This is dashboard Fragment"
    }
    val text: LiveData<String> = _text

    companion object{
        val chartList = mutableListOf(
            ScaleInfo("0", 71.3f, "1/10"),
            ScaleInfo("0", 70.0f, "1/11"),
            ScaleInfo("0", 71.5f, "1/12"),
            ScaleInfo("0", 70.2f, "1/13"),
            ScaleInfo("0", 72.6f, "1/14"),
        )
    }

    fun getScaleList(){
        chartItems.value = chartList
    }
}