package com.cvnet.smartmirror.ui.scale.result.model

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.cvnet.smartmirror.MainViewModel
import com.cvnet.smartmirror.R
import com.cvnet.smartmirror.databinding.ItemHealthInfoBinding
import com.cvnet.smartmirror.ui.scale.result.ScaleResultViewModel

class HealthInfoAdapter(val viewModel:ScaleResultViewModel?, val context:Context) : RecyclerView.Adapter<HealthInfoAdapter.ViewHolder>() {

    val items = mutableListOf<HealthInfo>()
    init{
        viewModel?.healthInfoItems?.second?.observeForever{
            val clear = viewModel.healthInfoItems.first.get() ?: false
            if(clear) items.clear()
            val list = it.filter{ healthInfo -> !items.contains(healthInfo) }
            items.addAll(list)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val binding  = ItemHealthInfoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding, viewModel, context)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        items.run{ holder.bind(this[position])}
    }

    override fun getItemCount(): Int  = items.size

    class ViewHolder(
        val binding: ItemHealthInfoBinding,
        val viewModel:ScaleResultViewModel?,
        val context:Context
    ):RecyclerView.ViewHolder(binding.root){

        fun bind(healthInfo:HealthInfo){

            when(healthInfo.title){
                R.string.health_info_fat.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_bmi.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_water_rate.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_bmr.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_bone_volume.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_muscle_volume.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
                R.string.health_info_visceral_fat.toString() -> binding.imgIcon.setBackgroundResource(R.drawable.ic_dashboard_black_24dp)
            }

            binding.txtTitle.text = healthInfo.title
            binding.txtStandard.text = healthInfo.standard
            binding.txtData.text = healthInfo.healthInfo
            binding.txtEvaluation.text = healthInfo.evaluation
            if(healthInfo.type == ""){
                binding.txtEvaluation.setBackgroundResource(R.drawable.bg_round_solid_red)
            }
        }
    }
}