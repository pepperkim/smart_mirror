package com.cvnet.smartmirror.ui.scale.connect

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.cvnet.smartmirror.MainViewModel
import com.cvnet.smartmirror.R
import com.cvnet.smartmirror.databinding.FragmentScaleConnectBinding
import com.cvnet.smartmirror.ui.home.HomeViewModel
import com.ebelter.scaleblesdk.ScaleBleManager
import kotlinx.coroutines.runBlocking

class ScaleConnectFragment : Fragment() {

    private val mainViewModel by activityViewModels<MainViewModel>()
    private lateinit var viewModel: ScaleConnectViewModel

    private var _binding : FragmentScaleConnectBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewModel =
            ViewModelProvider(this).get(ScaleConnectViewModel::class.java)

        _binding = FragmentScaleConnectBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.scanProgress.visibility = View.VISIBLE
        binding.scanProgress.animate()

        viewModel.scaleMeasureState.observe(viewLifecycleOwner){
            when(it) {
                ScaleConnectViewModel.ScaleMeasureState.Search -> binding.txtMain.text = getString(R.string.sentence_connect_scale)
                ScaleConnectViewModel.ScaleMeasureState.Connect -> {
                    binding.scanProgress.visibility = View.GONE
                    binding.scanProgress.clearAnimation()
                    binding.txtMain.text = getString(R.string.sentence_connected_scale)
                }
                ScaleConnectViewModel.ScaleMeasureState.Measure -> {
//                    binding.txtMain.text = getString(R.string.sentence_measuring_desc)

                    mainViewModel.publicMeasure.value = viewModel.measureResult.value
//                    viewModel.disconnectBleScale()

                    findNavController().navigate(R.id.action_connect_to_result)
                }

                ScaleConnectViewModel.ScaleMeasureState.Weight -> {
                    binding.txtMain.text = getString(R.string.sentence_connect_scale)
                }
                else -> {}
            }
        }

        viewModel.searchBleScale()
    }

    override fun onPause() {
        super.onPause()
        viewModel.disconnectBleScale()
    }
}