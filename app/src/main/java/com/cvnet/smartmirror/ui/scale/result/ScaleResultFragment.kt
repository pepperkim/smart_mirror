package com.cvnet.smartmirror.ui.scale.result

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cvnet.smartmirror.MainViewModel
import com.cvnet.smartmirror.databinding.FragmentScaleResultBinding
import com.cvnet.smartmirror.ui.scale.result.model.HealthInfoAdapter
import com.cvnet.smartmirror.utils.chart.MyValueFormatter
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import kotlinx.coroutines.runBlocking

class ScaleResultFragment : Fragment() {

    protected val mainViewModel by activityViewModels<MainViewModel>()

    private lateinit var viewModel: ScaleResultViewModel
    private var _binding : FragmentScaleResultBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        viewModel = ViewModelProvider(this)[ScaleResultViewModel::class.java]
        _binding = FragmentScaleResultBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUI()
        setDataBindig()
    }

    private fun setUI(){

        binding.rcvHealthInfo.adapter = HealthInfoAdapter(viewModel, requireContext())

    }

    private fun setDataBindig(){

        viewModel.chartItems.observe(viewLifecycleOwner, Observer {
            drawChart(binding.lineChart, viewModel.getEntryData(), false)
            drawChart(binding.lineChart2, viewModel.getEntryData(), true)
        })

        viewModel.measureResult.value = mainViewModel.publicMeasure.value

        viewModel.getScaleList()
        viewModel.setHealthInfo()
    }

    private fun drawChart(chart: LineChart, chartData: ArrayList<Entry>, hide: Boolean){

        chart.setDrawGridBackground(false)
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        chart.setTouchEnabled(false)

        chart.axisLeft.isEnabled = false
        chart.axisRight.isEnabled = false
        chart.xAxis.isEnabled = false
        chart.setExtraOffsets(0f,0f,0f,0f)

        val xAxisList = ArrayList<String>()

        for (i in 0..6) {
            xAxisList.add("")
        }

        val lineDataSet = LineDataSet(chartData, "")
        chart.animate()
        val lineData = LineData(xAxisList, lineDataSet)
        lineData.setValueTextColor(Color.BLACK)
        if(hide){
            lineDataSet.color = Color.TRANSPARENT
            lineDataSet.setCircleColor(Color.TRANSPARENT)
        }else{
            lineDataSet.setColors(viewModel.getChartColors())
            lineDataSet.setCircleColors(viewModel.getChartColors())
        }

        lineDataSet.circleRadius = 10f
        lineDataSet.valueTextSize = 15f
        lineDataSet.setCircleColorHole(Color.TRANSPARENT)
        lineDataSet.valueFormatter = MyValueFormatter(hide)

        chart.data = lineData
        chart.setExtraOffsets(0f,40f, 0f,0f)
        chart.highlightValue(null)
        chart.legend.isEnabled = false
        chart.setDescription("")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        super.onOptionsItemSelected(item)
        activity?.onBackPressed()
        return true
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}