package com.cvnet.smartmirror.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cvnet.smartmirror.api.model.user.UserModel
import com.cvnet.smartmirror.repo.MemStore
import com.cvnet.smartmirror.repo.UserDB
import com.cvnet.smartmirror.repo.entity.UserEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    val userItems = MutableLiveData<MutableList<UserModel>>()

    enum class DBControlState{
        Success,
        Insert,
        Delete,
        Update,
        Fail
    }

    val dbControlState = MutableLiveData<DBControlState>()

    fun getUserFromDB(){
        viewModelScope.launch(Dispatchers.IO){
            UserDB.db.userDao().runCatching { findAll() }
                .onFailure { Timber.wtf(it) }
                .onSuccess { userList ->
                    val list = userList.map {
                        UserModel(
                            id = it.id,
                            name = it.name,
                            age = it.age,
                            role = it.role)
                    }

                    userItems.value = list.toMutableList()
                }
        }
    }

    fun insertUser(_name:String = "master", _age:Int = 20, _role:String = "admin"){
        viewModelScope.launch(Dispatchers.IO){
            UserDB.db.userDao().runCatching { insert(
                    UserEntity(_name, _age, _role)
                )}
                .onFailure {
                    Timber.wtf("insertUser onFailure $it")
                    dbControlState.postValue(DBControlState.Fail)
                }
                .onSuccess {
                    dbControlState.postValue(DBControlState.Success)
                }
        }
    }

    fun deleteUser(_id:Long){
        viewModelScope.launch(Dispatchers.IO) {
            UserDB.db.userDao().runCatching { deleteById(_id) }
                .onFailure {
                    Timber.wtf("deleteUser Failed. $it")
                    dbControlState.postValue(DBControlState.Fail)
                }
                .onSuccess {
                    dbControlState.postValue(DBControlState.Success)
                }
        }
    }
}