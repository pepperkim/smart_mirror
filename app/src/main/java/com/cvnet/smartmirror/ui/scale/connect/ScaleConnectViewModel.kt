package com.cvnet.smartmirror.ui.scale.connect

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.cvnet.smartmirror.repo.MemStore
import com.cvnet.smartmirror.repo.UserDB
import com.cvnet.smartmirror.repo.entity.ScaleEntity
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.cvnet.smartmirror.utils.DateUtils
import com.ebelter.scaleblesdk.ScaleBleManager
import com.ebelter.scaleblesdk.ble.bluetooth.callback.ConnectCallback
import com.ebelter.scaleblesdk.ble.bluetooth.impl.IMeasureResultCallback
import com.ebelter.scaleblesdk.ble.bluetooth.impl.IUserInfoChangedCallback
import com.ebelter.scaleblesdk.model.MeasureResult
import com.ebelter.scaleblesdk.model.OfflineMeasureResult
import com.ebelter.scaleblesdk.model.ScaleUser
import com.ebelter.scaleblesdk.model.Weight
import com.fourlab.roomtest.repo.Pref
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import timber.log.Timber

class ScaleConnectViewModel : ViewModel() {
    var sbm: ScaleBleManager? = null

    val user: UserEntity? = null

    val scaleMeasureState = MutableLiveData<ScaleMeasureState>()
    val measureResult = MutableLiveData<MeasureResult>()
    val weightResult = MutableLiveData<Weight>()

    enum class ScaleMeasureState{
        Ready,
        Search,
        Connect,
        Weight,
        Measure,
        Complete,
        DisConnect,
        DBUpdateFail,
        Error
    }

    // 체중계 검색
    fun searchBleScale(){

        sbm = ScaleBleManager.getInstance()
        sbm?.registerConnectCallback(mConnectCallback)
        sbm?.registerUserInfoChangedCallback(mUserInfoChangedCallback)
        sbm?.registerMeasureResultCallback(mMeasureResultCallback)

        sbm?.startScan {
            Timber.d("detected scan device name = ${it.name}, address =  ${it.address}")
            scaleMeasureState.postValue(ScaleMeasureState.Search)
            sbm?.startConnect(it)
        }
    }

    // 체중계 연결 정보 콜백
    private val mConnectCallback = object : ConnectCallback {
        override fun onConnected(p0: BluetoothDevice?) {
            Timber.d("-- onConnected --")
            scaleMeasureState.postValue(ScaleMeasureState.Connect)

            if (p0 != null) {
                Pref.Instance.isPaired = true
                Pref.Instance.pairDevice = p0.address
                MemStore.ScaleDevice.updateDevice(p0)
            }

//            binding.txtMain.text = getString(R.string.sentence_connected_scale)

            // 사용자 정보 입력
            val user = ScaleUser.getUser()
            user.nike = "master"
            user.btId = ScaleUser.toBtId("00001")
            user.age = 20
            user.sex = 0
            user.weight = 78.0f
//            user.target = 1f
            user.impedance = 244.0f
            user.height = 178
            user.roleType = 1

            sbm?.sendUserInfo(user)


        }

        override fun onScaleWake() { Timber.d("-- onScaleWake --") }

        override fun onScaleSleep() { Timber.d("-- onScaleSleep --") }

        override fun onDisConnected() {
            scaleMeasureState.postValue(ScaleMeasureState.DisConnect)
            Timber.d("-- onDisConnected --") }
    }

    // 사용자 정보 변경 콜백
    private val mUserInfoChangedCallback = object : IUserInfoChangedCallback {
        override fun onUserInfoUpdateSuccess() { Timber.d("-- onUserInfoUpdateSuccess --") }

        override fun onDeleteAllUsersSuccess() { Timber.d("-- onDeleteAllUsersSuccess --") }
    }

    private fun update():Int{
        return runBlocking(Dispatchers.IO) {
            val scale = UserDB.db.scaleDao().findOneToday(
//                user?.id!!, 유저 정보 임시 설정
                0,
                DateUtils.getTimeStampToDate(System.currentTimeMillis(), true)
            ) ?: return@runBlocking ScaleEntity(
                date = DateUtils.getTimeStampToDate(System.currentTimeMillis(), true),
                weight = measureResult.value?.weight,
                userId = 0,
                scaleId = 0).let{ UserDB.db.scaleDao().insert(it).toInt()}

            return@runBlocking scale.copy(
                date = DateUtils.getTimeStampToDate(System.currentTimeMillis(), true),
                weight = measureResult.value?.weight,
                userId = 0,
                scaleId = 0
            ).let { UserDB.db.scaleDao().update(it) }
        }
    }

    // 체중계 측정 정보 콜백
    private val mMeasureResultCallback = object : IMeasureResultCallback {
        override fun onReceiveMeasureResult(p0: MeasureResult?) {
            Timber.d("-- onReceiveMeasureResult --")

            p0?.let{
                Timber.d("measureResult btid = ${it.btId}")
                Timber.d("measureResult age = ${it.age}")
                Timber.d("measureResult sex = ${it.sex}")
                Timber.d("measureResult resistance = ${it.resistance}")
                Timber.d("measureResult weight = ${it.weight}")
                Timber.d("measureResult fat = ${it.fat}")
                Timber.d("measureResult bmi = ${it.bmi}")
                Timber.d("measureResult waterRate = ${it.waterRate}")
                Timber.d("measureResult bmr = ${it.bmr}")
                Timber.d("measureResult boneVolume = ${it.boneVolume}")
                Timber.d("measureResult muscleVolume = ${it.muscleVolume}")
                Timber.d("measureResult visceralFat = ${it.visceralFat}")
                Timber.d("measureResult protein = ${it.protein}")
                Timber.d("measureResult weightUnit = ${it.weightUnit}")
                Timber.d("measureResult fatUnit = ${it.fatUnit}")

                runBlocking {
                    measureResult.postValue(it)
                }

                /*update().also{ query ->
                    if( query == 0){
                        scaleMeasureState.postValue(ScaleMeasureState.DBUpdateFail)
                    }else{
                        scaleMeasureState.postValue(ScaleMeasureState.Measure)
                    }
                }*/

                scaleMeasureState.postValue(ScaleMeasureState.Measure)
            }





//            activity?.runOnUiThread(Runnable {
//                _binding?.txtMain?.append("onReceiveMeasureResult : ${p0.toString()} \n")
//            })

//            val btId: String? = null // 블루투스 아이디
//            val age = 0 // 나이
//            val sex = 0 // 성별

//            val mac: String? = null // ?
//            val measureTime: String? = null // ?
//            val resistance = 0f // ?

//            val weight = 0f // 체중

//            val fat = 0f // 체지방
//            val bmi = 0f // bmi
//            val waterRate = 0f // 체수분
//            val bmr = 0f // 기초대사량

//            val boneVolume = 0f // 뼈 볼륨 - 골밀도?
//            val muscleVolume = 0f // 근육량
//            val visceralFat = 0f // 내장 지방

//            val protein = 0f // 단백질
//            val weightUnit = "kg" // 체중 단위
//            val fatUnit = "%" // 체지방 단위

//            CoroutineScope(Dispatchers.Main).launch{
//                _binding?.txtMain?.append("onReceiveMeasureResult : ${p0.toString()} \n")
//            }
        }

        override fun onWeightMeasureResult(p0: Weight?) {
            Timber.d("-- onWeightMeasureResult --")

            p0.let{
                weightResult.postValue(it)
            }
            scaleMeasureState.postValue(ScaleMeasureState.Weight)

//            CoroutineScope(Dispatchers.Main).launch{
//                _binding?.txtMain?.append("onWeightMeasureResult : ${p0.toString()} \n")
//            }
        }

        override fun onWeightOverLoad() {
            Timber.d("-- onWeightOverLoad --")
        }

        override fun onReceiveHistoryRecord(p0: OfflineMeasureResult?) {
            Timber.d("-- onReceiveHistoryRecord --")
//            activity?.runOnUiThread(Runnable {
//                _binding?.txtMain?.append("onReceiveHistoryRecord : ${p0.toString()}\n")
//            })
        }

        override fun onFatMeasureError(p0: Int) {
            Timber.d("-- onFatMeasureError - type : $p0 --")
        }

        override fun onHistoryUploadDone() {
            Timber.d("-- onHistoryUploadDone --")
        }
    }

    fun disconnectBleScale(){
        sbm?.unbindDevice()
        sbm = null
    }
}