package com.cvnet.smartmirror.ui.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.cvnet.smartmirror.R
import com.cvnet.smartmirror.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private lateinit var homeViewModel : HomeViewModel
    private var _binding : FragmentHomeBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        homeViewModel =
                ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        binding.infoViewBottom.visibility = View.VISIBLE

        setUI()
        setBindData()
    }

    private fun setUI(){

        binding.txtNowClock.setOnClickListener {
            homeViewModel.insertUser("master", 20, "admin")
        }

        binding.txtNowDate.setOnClickListener {
            homeViewModel.deleteUser(0)
        }

        binding.txtTodayComment.setOnClickListener {
            homeViewModel.getUserFromDB()
        }
    }

    private fun setBindData(){
        homeViewModel.userItems.observe(viewLifecycleOwner){
            Toast.makeText(requireContext(), "유저 정보 조회 성공", Toast.LENGTH_SHORT).show()
            binding.txtTodayComment.text =  it.size.toString()
        }

        homeViewModel.dbControlState.observe(viewLifecycleOwner){
            when(it){
                HomeViewModel.DBControlState.Fail -> binding.txtTodayComment.text = getString(R.string.sentence_connect_scale)
                HomeViewModel.DBControlState.Success->binding.txtTodayComment.text = getString(R.string.sentence_connect_scale)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}