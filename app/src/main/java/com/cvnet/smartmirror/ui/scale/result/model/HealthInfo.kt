package com.cvnet.smartmirror.ui.scale.result.model

data class HealthInfo(
    val id : String? = null,

    val title:String? ="",
    val type:String? = "",
    val standard:String? = "",
    val healthInfo:String? = "",
    val evaluation:String? = "",
    val level: Int? = 0,

    /*
    * icon
    * title
    * standard
    * data
    * contents
    * state
    * */

    val age:Int? = 0, // 나이
    val sex:Int? = 0, // 성별
    val mac: String? = null, // ?
    val measureTime: String? = null, // ?
    val resistance:Float? = 0f, // ?
    val fat:Float? = 0f, // 체지방
    val weight:Float? = 0f, // 체중
    val waterRate:Float? = 0f, // 체수분
    val bmr:Float? = 0f, // 기초대사량
    val visceralFat:Float? = 0f, // 내장 지방
    val muscleVolume:Float? = 0f, // 근육량
    val boneVolume:Float? = 0f, // 뼈 볼륨 - 골밀도?
    val bmi:Float? = 0f, // bmi
    val protein:Float? = 0f, // 단백질
    val weightUnit: String? = "kg", // 체중 단위
    val fatUnit: String?= "%", // 체지방 단위
)