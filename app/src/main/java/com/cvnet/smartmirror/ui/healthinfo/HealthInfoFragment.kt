package com.cvnet.smartmirror.ui.healthinfo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.cvnet.smartmirror.R
import com.cvnet.smartmirror.databinding.FragmentHealthInfoBinding

class HealthInfoFragment : Fragment() {

    private lateinit var healthInfoViewModel:HealthInfoViewModel
    private var _binding:FragmentHealthInfoBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        healthInfoViewModel = ViewModelProvider(this).get(HealthInfoViewModel::class.java)

        _binding = FragmentHealthInfoBinding.inflate(inflater, container, false)
        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUI()
        setDataBinding()
    }

    private fun setUI(){

    }

    private fun setDataBinding(){

        healthInfoViewModel.chartItems.observe(viewLifecycleOwner, Observer {

        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}