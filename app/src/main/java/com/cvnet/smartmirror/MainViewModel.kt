package com.cvnet.smartmirror

import android.bluetooth.BluetoothDevice
import android.graphics.Color
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.cvnet.smartmirror.repo.MemStore
import com.cvnet.smartmirror.repo.UserDB
import com.cvnet.smartmirror.repo.entity.ScaleEntity
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.cvnet.smartmirror.ui.scale.result.model.HealthInfo
import com.cvnet.smartmirror.ui.scale.result.model.ScaleInfo
import com.cvnet.smartmirror.utils.DateUtils
import com.cvnet.smartmirror.utils.HealthInfoEvaluation
import com.ebelter.scaleblesdk.ScaleBleManager
import com.ebelter.scaleblesdk.ble.bluetooth.callback.ConnectCallback
import com.ebelter.scaleblesdk.ble.bluetooth.impl.IMeasureResultCallback
import com.ebelter.scaleblesdk.ble.bluetooth.impl.IUserInfoChangedCallback
import com.ebelter.scaleblesdk.model.MeasureResult
import com.ebelter.scaleblesdk.model.OfflineMeasureResult
import com.ebelter.scaleblesdk.model.ScaleUser
import com.ebelter.scaleblesdk.model.Weight
import com.fourlab.roomtest.repo.Pref
import com.github.mikephil.charting.data.Entry
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import timber.log.Timber
import java.lang.NullPointerException

class MainViewModel : ViewModel() {

    val publicMeasure = MutableLiveData<MeasureResult>()

    var sbm: ScaleBleManager? = null

    val user:UserEntity? = null

    val scaleMeasureState = MutableLiveData<ScaleMeasureState>()
    val measureResult = MutableLiveData<MeasureResult>()
    val weightResult = MutableLiveData<Weight>()

    enum class ScaleMeasureState{
        Ready,
        Search,
        Connect,
        Weight,
        Measure,
        Complete,
        DisConnect,
        DBUpdateFail,
        Error
    }

    // 체중계 검색
    fun searchBleScale(){

        sbm = ScaleBleManager.getInstance()
        sbm?.registerConnectCallback(mConnectCallback)
        sbm?.registerUserInfoChangedCallback(mUserInfoChangedCallback)
        sbm?.registerMeasureResultCallback(mMeasureResultCallback)

        sbm?.startScan {
            Timber.d("detected scan device name = ${it.name}, address =  ${it.address}")
            scaleMeasureState.postValue(ScaleMeasureState.Search)
            sbm?.startConnect(it)
        }
    }

    // 체중계 연결 정보 콜백
    private val mConnectCallback = object : ConnectCallback {
        override fun onConnected(p0: BluetoothDevice?) {
            Timber.d("-- onConnected --")
            scaleMeasureState.postValue(ScaleMeasureState.Connect)

            if (p0 != null) {
                Pref.Instance.isPaired = true
                Pref.Instance.pairDevice = p0.address
                MemStore.ScaleDevice.updateDevice(p0)
            }

//            binding.txtMain.text = getString(R.string.sentence_connected_scale)

            // 사용자 정보 입력
            val user = ScaleUser.getUser()
            user.nike = "master"
            user.btId = ScaleUser.toBtId("00001")
            user.age = 20
            user.sex = 0
            user.weight = 78.0f
//            user.target = 1f
            user.impedance = 244.0f
            user.height = 178
            user.roleType = 1

            sbm?.sendUserInfo(user)


        }

        override fun onScaleWake() { Timber.d("-- onScaleWake --") }

        override fun onScaleSleep() { Timber.d("-- onScaleSleep --") }

        override fun onDisConnected() {
            scaleMeasureState.postValue(ScaleMeasureState.DisConnect)
            Timber.d("-- onDisConnected --") }
    }

    // 사용자 정보 변경 콜백
    private val mUserInfoChangedCallback = object : IUserInfoChangedCallback {
        override fun onUserInfoUpdateSuccess() { Timber.d("-- onUserInfoUpdateSuccess --") }

        override fun onDeleteAllUsersSuccess() { Timber.d("-- onDeleteAllUsersSuccess --") }
    }

    private fun update():Int{
        return runBlocking(Dispatchers.IO) {
            val scale = UserDB.db.scaleDao().findOneToday(
//                user?.id!!, 유저 정보 임시 설정
                0,
                DateUtils.getTimeStampToDate(System.currentTimeMillis(), true)
            ) ?: return@runBlocking ScaleEntity(
                date = DateUtils.getTimeStampToDate(System.currentTimeMillis(), true),
                weight = measureResult.value?.weight,
                userId = 0,
                scaleId = 0).let{UserDB.db.scaleDao().insert(it).toInt()}

            return@runBlocking scale.copy(
                date = DateUtils.getTimeStampToDate(System.currentTimeMillis(), true),
                weight = measureResult.value?.weight,
                userId = 0,
                scaleId = 0
            ).let { UserDB.db.scaleDao().update(it) }
        }
    }

    // 체중계 측정 정보 콜백
    private val mMeasureResultCallback = object : IMeasureResultCallback {
        override fun onReceiveMeasureResult(p0: MeasureResult?) {
            Timber.d("-- onReceiveMeasureResult --")

            p0?.let{
                Timber.d("measureResult btid = ${it.btId}")
                Timber.d("measureResult age = ${it.age}")
                Timber.d("measureResult sex = ${it.sex}")
                Timber.d("measureResult resistance = ${it.resistance}")
                Timber.d("measureResult weight = ${it.weight}")
                Timber.d("measureResult fat = ${it.fat}")
                Timber.d("measureResult bmi = ${it.bmi}")
                Timber.d("measureResult waterRate = ${it.waterRate}")
                Timber.d("measureResult bmr = ${it.bmr}")
                Timber.d("measureResult boneVolume = ${it.boneVolume}")
                Timber.d("measureResult muscleVolume = ${it.muscleVolume}")
                Timber.d("measureResult visceralFat = ${it.visceralFat}")
                Timber.d("measureResult protein = ${it.protein}")
                Timber.d("measureResult weightUnit = ${it.weightUnit}")
                Timber.d("measureResult fatUnit = ${it.fatUnit}")

                runBlocking {
                    measureResult.postValue(it)
                }

                /*update().also{ query ->
                    if( query == 0){
                        scaleMeasureState.postValue(ScaleMeasureState.DBUpdateFail)
                    }else{
                        scaleMeasureState.postValue(ScaleMeasureState.Measure)
                    }
                }*/

                scaleMeasureState.postValue(ScaleMeasureState.Measure)
            }





//            activity?.runOnUiThread(Runnable {
//                _binding?.txtMain?.append("onReceiveMeasureResult : ${p0.toString()} \n")
//            })

//            val btId: String? = null // 블루투스 아이디
//            val age = 0 // 나이
//            val sex = 0 // 성별

//            val mac: String? = null // ?
//            val measureTime: String? = null // ?
//            val resistance = 0f // ?

//            val weight = 0f // 체중

//            val fat = 0f // 체지방
//            val bmi = 0f // bmi
//            val waterRate = 0f // 체수분
//            val bmr = 0f // 기초대사량

//            val boneVolume = 0f // 뼈 볼륨 - 골밀도?
//            val muscleVolume = 0f // 근육량
//            val visceralFat = 0f // 내장 지방

//            val protein = 0f // 단백질
//            val weightUnit = "kg" // 체중 단위
//            val fatUnit = "%" // 체지방 단위

//            CoroutineScope(Dispatchers.Main).launch{
//                _binding?.txtMain?.append("onReceiveMeasureResult : ${p0.toString()} \n")
//            }
        }

        override fun onWeightMeasureResult(p0: Weight?) {
            Timber.d("-- onWeightMeasureResult --")

            p0.let{
                weightResult.postValue(it)
            }
            scaleMeasureState.postValue(ScaleMeasureState.Weight)

//            CoroutineScope(Dispatchers.Main).launch{
//                _binding?.txtMain?.append("onWeightMeasureResult : ${p0.toString()} \n")
//            }
        }

        override fun onWeightOverLoad() {
            Timber.d("-- onWeightOverLoad --")
        }

        override fun onReceiveHistoryRecord(p0: OfflineMeasureResult?) {
            Timber.d("-- onReceiveHistoryRecord --")
//            activity?.runOnUiThread(Runnable {
//                _binding?.txtMain?.append("onReceiveHistoryRecord : ${p0.toString()}\n")
//            })
        }

        override fun onFatMeasureError(p0: Int) {
            Timber.d("-- onFatMeasureError - type : $p0 --")
        }

        override fun onHistoryUploadDone() {
            Timber.d("-- onHistoryUploadDone --")
        }
    }

    fun disconnectBleScale(){
        sbm?.unbindDevice()
        sbm = null
    }

    //
    //
    //////  result  /////
    //
    //

    var scaleEntityList = ArrayList<ScaleEntity>()
    val chartItems = MutableLiveData<MutableList<ScaleInfo>>()

    val entryItems = MutableLiveData<MutableList<Entry>>()

    val healthInfoItems = Pair<ObservableField<Boolean>,
            MutableLiveData<MutableList<HealthInfo>>>(ObservableField(false), MutableLiveData(mutableListOf()))

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    companion object{
        val chartList = mutableListOf(
            ScaleInfo("0", 71.3f, "1/10"),
            ScaleInfo("0", 70.0f, "1/11"),
            ScaleInfo("0", 71.5f, "1/12"),
            ScaleInfo("0", 70.2f, "1/13"),
            ScaleInfo("0", 72.6f, "1/14"),
        )

        var healthInfos = mutableListOf(
            HealthInfo("체지방률", "1","", "10.1%", "10.1%", "높은 편이에요"),
            HealthInfo("BMI", "1","", "22.4", "22.4", "높은 편이에요"),
            HealthInfo("체수", "1","", "50 ~ 65%", "40.2%", "조금 부족요"),
            HealthInfo("기초대사량", "0","", "1367.4 ~ 1971.6kcal", "1730kcal", "표준이에요"),
            HealthInfo("골격량", "0","", "2.7 ~ 3.7kg", "3.9kg", "표준보다 높아요"),
            HealthInfo("근육량", "0","", "49.4kg 이상", "59.1kg", "정상이에요"),
            HealthInfo("내장지방", "1","", "10 Level 이하", "13.5 Level", "조금 높아요")
        )
    }

    fun setHealthInfo(){

        //measureResult
//        val btId: String? = null // 블루투스 아이디
//            val age = 0 // 나이
//            val sex = 0 // 성별

//            val mac: String? = null // ?
//            val measureTime: String? = null // ?
//            val resistance = 0f // ?

//            val weight = 0f // 체중

//            val fat = 0f // 체지방
//            val bmi = 0f // bmi
//            val waterRate = 0f // 체수분
//            val bmr = 0f // 기초대사량

//            val boneVolume = 0f // 뼈 볼륨 - 골밀도?
//            val muscleVolume = 0f // 근육량
//            val visceralFat = 0f // 내장 지방

//            val protein = 0f // 단백질
//            val weightUnit = "kg" // 체중 단위
//            val fatUnit = "%" // 체지방 단위

        val fatLevel = HealthInfoEvaluation.getStandardST(measureResult.value?.sex!! , measureResult.value?.age!!, measureResult.value?.fat!!, HealthInfoEvaluation.fatStandardList )
        val bmiLevel = HealthInfoEvaluation.getStandardBMI( measureResult.value?.bmi!! )
        val waterLevel = HealthInfoEvaluation.getStandardST(measureResult.value?.sex!! , measureResult.value?.age!!, measureResult.value?.waterRate!!, HealthInfoEvaluation.waterRateStandardDataList )
        val bmrLevel = HealthInfoEvaluation.getStandardST(measureResult.value?.sex!! , measureResult.value?.age!!, measureResult.value?.bmr!!, HealthInfoEvaluation.bmrStandardDataList)
        val boneLevel = HealthInfoEvaluation.getStandardST(measureResult.value?.sex!! , measureResult.value?.age!!, measureResult.value?.boneVolume!!, HealthInfoEvaluation.boneStandardDataList)
        val muscleLevel = HealthInfoEvaluation.getStandardST(measureResult.value?.sex!! , measureResult.value?.age!!, measureResult.value?.muscleVolume!!, HealthInfoEvaluation.muscleStandardDataList)
        val visceralFatLevel = HealthInfoEvaluation.getStandardVisceralFat( measureResult.value?.visceralFat)

        healthInfoItems.second.postValue(mutableListOf(
            HealthInfo("체지방률", "체지방률","", "10.1%", measureResult.value?.fat.toString() + " %", "높은 편이에요", fatLevel),
            HealthInfo("BMI", "BMI","", "22.4", measureResult.value?.bmi.toString(), "높은 편이에요", bmiLevel),
            HealthInfo("체수분", "체수분","", "50 ~ 65%", measureResult.value?.waterRate.toString() + "%", "조금 부족요", waterLevel),
            HealthInfo("기초대사량", "0","", "1367.4 ~ 1971.6kcal", measureResult.value?.bmr.toString() +"kcal", "표준이에요", bmrLevel),
            HealthInfo("골격량", "0","", "2.7 ~ 3.7kg", measureResult.value?.boneVolume.toString()+"kg", "표준보다 높아요", boneLevel),
            HealthInfo("근육량", "0","", "49.4kg 이상", measureResult.value?.muscleVolume.toString()+"kg", "정상이에요", muscleLevel),
            HealthInfo("내장지방", "1","", "10 Level 이하", measureResult.value?.visceralFat.toString()+" Level", "조금 높아요", visceralFatLevel)
        ))
    }

    fun getUserInfo(){
        viewModelScope.launch(Dispatchers.IO) {
            MemStore.User.updateUserInfo(UserDB.db.userDao().findOneName(""))
        }
    }

    fun getScaleList(){

//        viewModelScope.launch(Dispatchers.IO){
//            UserDB.db.scaleDao().runCatching { findAll(MemStore.User.userId) }
//                .onFailure { Timber.wtf(it) }
//                .onSuccess { scaleList ->
//                    var list = scaleList.map {
//                        ScaleInfo(
//                            user_id = it.userId.toString(),
//                            scale_num = it.weight,
//                            scale_date = it.date  )
//                    }
//                    if(list.size > 5){
//                        list = list.subList(0,5)
//                    }
//
//                    chartItems.value = list.toMutableList()
//                }
//        }

        chartItems.value = chartList
    }

    fun getEntryData():ArrayList<Entry>{
        val list = ArrayList<Entry>()
        for (i in 1 until chartItems.value!!.size) {
            val scaleItem = chartItems.value!![i]
            list.add(Entry(scaleItem.scale_num!!, i, scaleItem.scale_date))
        }
        return list
    }

    fun getChartColors(): IntArray {
        try{
            val colors = IntArray(chartItems.value!!.size)
            colors[0] = Color.rgb(51, 181, 229)  // 시작 블루
            for (i in 1 until chartItems.value!!.size) {
                val beforeInfo: ScaleInfo = chartItems.value!![i - 1]
                val nowInfo: ScaleInfo = chartItems.value!![i]
                if (beforeInfo.scale_num!! > nowInfo.scale_num!!) { // 체중 감소색 녹색
                    colors[i] = Color.rgb(106, 150, 31)
                } else { // 체중 증가 주황
                    colors[i] = Color.rgb(255, 102, 0)
                }
            }
            return colors
        }catch (ext: NullPointerException){
            return IntArray(0)
        }
    }
}