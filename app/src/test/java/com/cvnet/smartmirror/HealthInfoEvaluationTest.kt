package com.cvnet.smartmirror

import com.google.common.truth.Truth.assertThat
import com.cvnet.smartmirror.utils.HealthInfoEvaluation
import org.junit.Before
import org.junit.Test

class HealthInfoEvaluationTest {

    private lateinit var hie: HealthInfoEvaluation

    @Before
    fun setUp(){
        hie = HealthInfoEvaluation
    }
    
    @Test
    fun getScaleStandardTest(){
        val result = hie.getScaleStandard(178, 78)
        assertThat(result).isEqualTo(1)
    }

    @Test
    fun getStandardSTTest(){
        val result = hie.getStandardST(1, 19, 22f,  hie.fatStandardList)
//        val result = hie.getStandardST(1,19, 1000.5f,  hie.bmrStandardDataList)
        assertThat(result).isEqualTo(0)
    }
}