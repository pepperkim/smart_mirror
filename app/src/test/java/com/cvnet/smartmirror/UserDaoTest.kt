package com.cvnet.smartmirror

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.cvnet.smartmirror.repo.UserDB
import com.cvnet.smartmirror.repo.dao.UserDao
import com.cvnet.smartmirror.repo.entity.UserEntity
import com.google.common.truth.Truth
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class UserDaoTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private lateinit var dao : UserDao
    private lateinit var _db : UserDB

    @Before
    fun setUp(){
        _db = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            UserDB::class.java
        ).build()
        dao = _db.userDao()
    }

    @After
    fun tearDown(){
        _db.close()
    }

    /*@Test
    fun saveUserTest() = runBlocking {
        val users = listOf(
            UserEntity("user1", 10, "master", 0),
            UserEntity("user2", 20, "member", 1),
            UserEntity("user3", 30, "guest", 2)
        )
        dao.insert(users)

        val allUsers = dao.findAll()
        Truth.assertThat(allUsers).isEqualTo(users)
    }*/
}