## Smart Mirror
* SDK 버전 : 31 
* 언어 : Kotlin
* 사용 기술
    * constraintlayout
    * livedata
    * lifecycle
    * navigation
    * coroutines
    * room
    * unit test  
</br>
* 외부 라이브러리
    * MPAndroidChart
    * retrofit2
    * Robolectric
    * tedpermission
    * glide
    * timber  
</br>